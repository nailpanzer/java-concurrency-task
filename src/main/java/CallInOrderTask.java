import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Задача: необходимо, чтобы методы вызывались в определенном порядке
 */
public class CallInOrderTask {
    public static void main(String[] args) {
        Foo foo = new Foo();

        Thread t1 = new Thread(() -> {
            try {
                foo.first(() -> System.out.println("1"));
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        });
        Thread t2 = new Thread(() -> {
            try {
                foo.second(() -> System.out.println("2"));
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        });
        Thread t3 = new Thread(() -> {
            try {
                foo.third(() -> System.out.println("3"));
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        });

        try {
            t3.start();
            Thread.sleep(100);
            t1.start();
            Thread.sleep(100);
            t2.start();
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    public static class Foo {

        private Lock lock;
        private Condition wrongMethodCalled;
        private int currentMethod;

        public Foo() {
            lock = new ReentrantLock();
            wrongMethodCalled = lock.newCondition();
            currentMethod = 1;
        }

        public void first(Runnable printFirst) throws InterruptedException {
            lock.lock();
            System.out.println("1 LOCK " + currentMethod);
            try {
                while (currentMethod != 1) {
                    wrongMethodCalled.await();
                }
                currentMethod = 2;

                printFirst.run();
            } finally {
                wrongMethodCalled.signalAll();
                lock.unlock();
                System.out.println("1 unlock");
            }
        }

        public void second(Runnable printSecond) throws InterruptedException {
            lock.lock();
            System.out.println("2 LOCK " + currentMethod);
            try {
                while (currentMethod != 2) {
                    wrongMethodCalled.await();
                }
                currentMethod = 3;
                printSecond.run();
            } finally {
                wrongMethodCalled.signalAll();
                lock.unlock();
                System.out.println("2 unlock");
            }
        }

        public void third(Runnable printThird) throws InterruptedException {
            lock.lock();
            System.out.println("3 LOCK " + currentMethod);
            try {
                while (currentMethod != 3) {
                    wrongMethodCalled.await();
                }
                currentMethod = 1;

                printThird.run();
            } finally {
                wrongMethodCalled.signalAll();
                lock.unlock();
                System.out.println("3 unlock");
            }
        }
    }
}
