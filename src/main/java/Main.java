import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class Main {
    public static void main(String[] args) {
        ExecutorService ex = Executors.newFixedThreadPool(10);

        CrptApi ourApi = CrptApi.create(TimeUnit.SECONDS, 3);

        List<Callable<Object>> tasks = new ArrayList<>();

        for (int i = 0; i < 20; i++) {
            String successPrint = String.format("Request %d finished", i);
            tasks.add(() -> {
                try {
                    ourApi.callApi(new CrptApi.Document("doc_id", "doc_status", "doc_type"), "sign");
                } catch (Exception e) {

                }
                System.out.println(successPrint);
                return null;
            });
        }

        try {
            ex.invokeAll(tasks);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }

        ex.shutdown();

        try {
            ex.awaitTermination(100, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }

        System.out.println();
    }
}
