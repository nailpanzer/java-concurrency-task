import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.concurrent.*;


/**
 * Задача: есть API, необходимо создать класс, который будет вызывать это API, но не чаще чем n раз в единицу времени
 */
public class CrptApi {
    private Semaphore semaphore;
    private TimeUnit timeUnit;
    private int requestsLimit;

    private CrptApi(TimeUnit timeUnit, int requestsLimit) {
        this.timeUnit = timeUnit;
        this.requestsLimit = requestsLimit;
        // fair=true чтобы гарантировать правильный порядок выполнения
        this.semaphore = new Semaphore(requestsLimit, true);
    }

    public static CrptApi create(TimeUnit timeUnit, int requestsLimit) {
        CrptApi instance = new CrptApi(timeUnit, requestsLimit);
        instance.startRender();
        return instance;
    }

    private void startRender() {
        Executors.newScheduledThreadPool(1, runnable -> {
            Thread t = Executors.defaultThreadFactory().newThread(runnable);
            t.setDaemon(true); // чтобы поток завершился, после завершения остальной программы
            return t;
        }).scheduleAtFixedRate(() -> {
            // каждую сукунду/минуту/час освобождаем семафор, чтобы следующие requestsLimit штук потоков прошли через acquire
            semaphore.release(requestsLimit);
            System.out.println("release");
        }, 2, 1, timeUnit);
    }

    public void callApi(Document document, String sign) {
        try {
            // ждем, пока не освободится место
            semaphore.acquire();
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }

        try {
            HttpClient client = HttpClient.newBuilder().build();
            HttpRequest request = HttpRequest.newBuilder().header("Content-Type", "application/json")
                    // URL апи, к которому идет запрос
                    // сейчас это просто эхо
                    .uri(URI.create("https://echo.free.beeceptor.com")).POST(HttpRequest.BodyPublishers.ofString(
                            // тело запроса, которое сериализовано в json объект
                            new ObjectMapper().writeValueAsString(new RequestBody(document, sign)))).build();

            // сразу же возвращает объект CompletableFuture, в котором скоро появится
            // результат запроса
            CompletableFuture<HttpResponse<String>> futureResponse = client.sendAsync(request,
                    HttpResponse.BodyHandlers.ofString());

            // ждем, пока прийдет ответ от API
            String result;
            result = futureResponse.get().body();

            // System.out.println("\tResult: " + result);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        } catch (ExecutionException e) {
            throw new RuntimeException(e);
        }
    }

    // схема для взаимодействия с API
    // аннотации нужны, чтобы при переводе в json поле переиминовалось в snake_case
    private record RequestBody(@JsonProperty("document") Document document, @JsonProperty("sign") String sign) {

    }

    // этот класс описывается отдельно, и спользуется в остальной программе
    public record Document(@JsonProperty("doc_id") String docId, @JsonProperty("doc_status") String docStatus,
                           @JsonProperty("doc_type") String docType) {

    }
}
